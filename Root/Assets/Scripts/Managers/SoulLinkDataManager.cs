﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Singleton;
using System;

public class SoulLinkDataManager : StaticSingleton<SoulLinkDataManager>
{
	public event Action<SoulLinkPairDataItem> OnSoulLinkItemAdded;
	public event Action<List<SoulLinkDataItem>> OnTeamUpdated;

	[Serializable]
	public class SoulLinkDataItem
	{
		[SerializeField]
		public string Item1;
		[SerializeField]
		public string Item2;

		private string _id;

		public string Id
		{
			get
			{
				if (_id.IsEmptyOrNull())
				{
					_id = Item1 + " / " + Item2;
				}
				return _id;
			}
		}
	}

	[Serializable]
	public class SoulLinkPairDataItem : SoulLinkDataItem
	{
		[SerializeField]
		public string Location;
		[SerializeField]
		public Player FirstEncounter;
	}

	private List<SoulLinkPairDataItem> _items = new List<SoulLinkPairDataItem>();
	private List<SoulLinkDataItem> _team = new List<SoulLinkDataItem>();

	public List<SoulLinkPairDataItem> Items { get { return _items; } }
	public List<SoulLinkDataItem> Team { get { return _team; } }

	public override void Init()
	{
		base.Init();
		_items = SaveGameManager.Instance.SoulLink.Data.items;
		_team = SaveGameManager.Instance.SoulLink.Data.team;
	}

	public void AddSoulLinkItem(SoulLinkPairDataItem item)
	{
		_items.Add(item);
		OnSoulLinkItemAdded.SafeInvoke(item);
		SaveGameManager.Instance.SoulLink.Data.UpdateItems(_items);
	}

	public void UpdateTeam(List<string> team)
	{
		_team.Clear();
		foreach (var item in team)
		{
			if (item != "None")
			{
				var pair = _items.Find(x => x.Id == item);
				_team.Add(pair);
			}
		}
		OnTeamUpdated.SafeInvoke(_team);
		SaveGameManager.Instance.SoulLink.Data.UpdateTeam(_team);
	}
}