﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectPool { public ObjectPool() { } }

public class ObjectPool<T> : ObjectPool where T : MonoBehaviour
{
    private List<T> _pool = new List<T>();
    private Transform _parent;
    private T _original;

    public ObjectPool(T original, Transform parent) : base()
    {
        _original = original;
        _parent = parent;
    }

    public T GetItem()
    {
		T item = null;
        if (_pool.Count == 0)
        {
            item = GameObject.Instantiate<T>(_original, _parent);
        }
        else
        {
            item = _pool[0];
            _pool.RemoveAt(0);
        }

        return item;
    }

    public void Release(T item)
    {
        _pool.Insert(0, item);
    }

	public void HidePooledItems()
	{
		foreach (var item in _pool)
		{
			item.gameObject.SetActive(false);
		}
	}
}