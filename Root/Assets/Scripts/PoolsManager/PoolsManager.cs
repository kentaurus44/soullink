﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Singleton;

public class PoolsManager : StaticSingleton<PoolsManager>
{
	[System.Serializable]
	public class PoolConfig
	{
		[SerializeField]
		private PoolSize _poolSize;
		[SerializeField]
		private bool _preload = true;
		[SerializeField]
		private bool _canOverLimit = false;

		public bool Preload { get { return _preload; } }
		public bool CanOverLimit { get { return _canOverLimit; } }
		public PoolSize PoolSize { get { return _poolSize; } }
	}

	private List<ObjectPool> _poolList = new List<ObjectPool>();
	
	public ObjectPool<T> CreatePool<T>(PoolConfig config, T defaultItem, Transform parent) where T : MonoBehaviour
	{
		var pool = new ObjectPool<T>(defaultItem, parent);
		_poolList.Add(pool);
		return pool;
	}

	public void Clear()
	{
		_poolList.Clear();
	}
}
