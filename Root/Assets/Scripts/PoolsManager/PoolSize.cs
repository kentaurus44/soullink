﻿using UnityEngine;

[System.Serializable]
public class PoolSize
{
	[SerializeField]
	private int _minSize;
	[SerializeField]
	private int _maxSize;
}
