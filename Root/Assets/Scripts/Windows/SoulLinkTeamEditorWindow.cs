﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkTeamEditorWindow : View
{
	[SerializeField]
	protected SoulLinkTeamEditorContainer _container;
	[SerializeField]
	protected Button _save;

	protected void Awake()
	{
		_save.onClick.AddListener(OnSaveCB);
	}

	public override void OnViewOpened()
	{
		base.OnViewOpened();
		_container.Init();
	}

	private void OnSaveCB()
	{
		List<string> items = new List<string>();
		foreach (var item in _container.Items)
		{
			items.Add(item.ItemSelected);
		}

		SoulLinkDataManager.Instance.UpdateTeam(items);
		SaveGameManager.Instance.SaveDirty();
	}
}
