﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SoulLinkAdderWindow : View
{
	[Serializable]
	protected class Item
	{
		[SerializeField]
		protected InputField _item;

		public InputField Input { get { return _item; } }
	}

	[SerializeField]
	protected Item _item1;
	[SerializeField]
	protected Item _item2;
	[SerializeField]
	protected InputField _location;
	[SerializeField]
	protected Dropdown _dropdown;
	[SerializeField]
	protected Button _button;

	private void Awake()
	{
		_item1.Input.onValueChanged.AddListener(OnTextChanged);
		_item2.Input.onValueChanged.AddListener(OnTextChanged);

		_location.onValueChanged.AddListener(OnTextChanged);
		_button.onClick.AddListener(OnButtonCB);
	}

	public override void OnViewOpened()
	{
		base.OnViewOpened();
		UpdateButtonState();
	}

	private void OnTextChanged(string text)
	{
		UpdateButtonState();
	}

	private void UpdateButtonState()
	{
		_button.interactable = !_item1.Input.text.IsEmptyOrNull() && !_item2.Input.text.IsEmptyOrNull() && !_location.text.IsEmptyOrNull();
	}

	private void OnButtonCB()
	{
		var item = new SoulLinkDataManager.SoulLinkPairDataItem();
		item.Item1 = _item1.Input.text;
		item.Item2 = _item2.Input.text;
		item.Location = _location.text;

		switch (_dropdown.value)
		{
			case (int)Player.None:
				item.FirstEncounter = Player.None;
				break;
			case (int)Player.Player1:
				item.FirstEncounter = Player.Player2;
				break;
			case (int)Player.Player2:
				item.FirstEncounter = Player.Player2;
				break;
		}

		SoulLinkDataManager.Instance.AddSoulLinkItem(item);
		_item1.Input.text = string.Empty;
		_item2.Input.text = string.Empty;
		_location.text = string.Empty;
	}
}
