﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulLinkWindow : View
{
	[SerializeField]
	protected SoulLinkItemsContainer _soulLinkItemsContainer;
	[SerializeField]
	protected SoulLinkTeamContainer _soulLinkTeamContainer;

	public override void OnViewOpened()
	{
		base.OnViewOpened();
		_soulLinkItemsContainer.Init();
		_soulLinkTeamContainer.Init();
	}
}