﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PersistentObject
{
	bool isDirty { get; set; }
}
