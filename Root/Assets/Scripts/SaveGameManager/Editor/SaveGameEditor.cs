﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveGameEditor
{
	[MenuItem("Kentaurus/PersistentData/Clear All")]
	public static void ClearPersistentData()
	{
		PlayerPrefs.DeleteAll();
	}
}
