﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfflineObject<T> where T : PersistentObject, new()
{
	private string _stateKey;

	public T Data { get; private set; }

	public void Load()
	{
		if (_stateKey.IsEmptyOrNull())
		{
			_stateKey = string.Format("{0}.{1}", Application.productName, typeof(T).Name);
		}

		var saved = PlayerPrefs.GetString(_stateKey, string.Empty);

		if (saved.IsEmptyOrNull())
		{
			Data = new T();
			Core.Debug.Log(ProjectConstants.kSaveGameLogTag, string.Format("Loaded: {0} with nothing", _stateKey));
		}
		else
		{
			Data = JsonUtility.FromJson<T>(saved);
			Core.Debug.Log(ProjectConstants.kSaveGameLogTag, string.Format("Loaded: {0} with {1}", _stateKey, saved));
		}
	}

	public void Save()
	{
		if (Data.isDirty)
		{
			var str = JsonUtility.ToJson(Data, true);
			PlayerPrefs.SetString(_stateKey, str.ToString());
			Data.isDirty = false;
			Core.Debug.Log(ProjectConstants.kSaveGameLogTag, string.Format("Save: {0} with {1}", _stateKey, str));
		}
	}
}
