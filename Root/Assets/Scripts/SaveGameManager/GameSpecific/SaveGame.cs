﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SaveGameManager
{
	private OfflineObject<SoulLinkState> _soulLink = new OfflineObject<SoulLinkState>();
	
	public OfflineObject<SoulLinkState> SoulLink
	{
		get { return _soulLink; }
	}

	partial void Load()
	{
		_soulLink.Load();
	}

	partial void Save()
	{
		_soulLink.Save();
	}
}
