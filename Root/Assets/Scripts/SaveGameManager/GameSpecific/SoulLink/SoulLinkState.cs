﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulLinkState : SoulLinkPersistent
{
	public List<SoulLinkDataManager.SoulLinkPairDataItem> items = new List<SoulLinkDataManager.SoulLinkPairDataItem>();
	public List<SoulLinkDataManager.SoulLinkDataItem> team = new List<SoulLinkDataManager.SoulLinkDataItem>();

	private bool isDirty;

	bool PersistentObject.isDirty
	{
		get
		{
			return isDirty;
		}
		set
		{
			isDirty = value;
		}
	}

	List<SoulLinkDataManager.SoulLinkPairDataItem> SoulLinkPersistent.items
	{
		get
		{
			return items;
		}
	}

	List<SoulLinkDataManager.SoulLinkDataItem> SoulLinkPersistent.team
	{
		get
		{
			return team;
		}
	}

	public void UpdateItems(List<SoulLinkDataManager.SoulLinkPairDataItem> items, List<SoulLinkDataManager.SoulLinkDataItem> team)
	{
		UpdateItems(items);
		UpdateTeam(team);
	}

	public void UpdateItems(List<SoulLinkDataManager.SoulLinkPairDataItem> items)
	{
		this.items = items ?? new List<SoulLinkDataManager.SoulLinkPairDataItem>();
		isDirty = true;
	}

	public void UpdateTeam(List<SoulLinkDataManager.SoulLinkDataItem> team)
	{
		this.team = team ?? new List<SoulLinkDataManager.SoulLinkDataItem>();
		isDirty = true;
	}
}