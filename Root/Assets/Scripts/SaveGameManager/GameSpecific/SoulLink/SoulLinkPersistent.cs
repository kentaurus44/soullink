﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SoulLinkPersistent : PersistentObject
{
	List<SoulLinkDataManager.SoulLinkPairDataItem> items { get; }
	List<SoulLinkDataManager.SoulLinkDataItem> team { get; }
}