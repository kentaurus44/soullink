﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Singleton;

public partial class SaveGameManager : StaticSingleton<SaveGameManager>
{
	partial void Load();
	partial void Save();

	public override void Init()
	{
		base.Init();
		Load();
	}

	public void SaveDirty()
	{
		Save();
	}
}
