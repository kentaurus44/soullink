﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulLinkTeamEditorContainer : MonoBehaviour
{
	[SerializeField]
	protected List<SoulLinkTeamEditorItem> _items = new List<SoulLinkTeamEditorItem>();

	public List<SoulLinkTeamEditorItem> Items
	{
		get
		{
			return _items;
		}
	}

	public void Init()
	{
		List<string> list = new List<string>() { "None" };
		foreach (var item in SoulLinkDataManager.Instance.Items)
		{
			list.Add(item.Id);
		}

		for (int i = 0; i < _items.Count; ++i)
		{
			var pair = i < SoulLinkDataManager.Instance.Team.Count ? SoulLinkDataManager.Instance.Team[i] : null;
			_items[i].Init(string.Format("Pair {0}", i + 1), list, pair);
		}
	}
}
