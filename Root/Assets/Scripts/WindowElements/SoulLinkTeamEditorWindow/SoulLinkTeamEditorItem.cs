﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkTeamEditorItem : Item
{
	[SerializeField]
	protected Text _label;
	[SerializeField]
	protected Dropdown _dropdown;

	private SoulLinkDataManager.SoulLinkDataItem _pair;
	private List<string> _options = new List<string>();
	private string _labelName;
	private int _indexSelected = 0;

	public string ItemSelected
	{
		get
		{
			return _dropdown.options[_indexSelected].text;
		}
	}

	protected void Awake()
	{
		_dropdown.onValueChanged.AddListener(OnValueChanged);
	}

	public void Init(string labelName, List<string> options, SoulLinkDataManager.SoulLinkDataItem pair = null)
	{
		Clear();
		_labelName = labelName;
		_pair = pair;
		_options = options;
		Init();
	}

	protected override void UpdateDynamic() { }

	protected override void UpdateStatic()
	{
		_label.text = _labelName;

		_dropdown.ClearOptions();

		var options = new List<string>(_options);

		if (_pair != null)
		{
			_indexSelected = options.IndexOf(_pair.Id);
		}

		_dropdown.AddOptions(options);
		_dropdown.value = _indexSelected;

	}

	public override void Clear()
	{
		_pair = null;
		_labelName = null;
		_options = null;
	}

	private void OnValueChanged(int index)
	{
		_indexSelected = index;
	}
}
