﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkActionBar : MonoBehaviour
{
	[SerializeField]
	protected Button _save;

	protected void Awake()
	{
		_save.onClick.AddListener(OnSaveCB);
	}

	protected void OnSaveCB()
	{
		SaveGameManager.Instance.SaveDirty();
	}
}
