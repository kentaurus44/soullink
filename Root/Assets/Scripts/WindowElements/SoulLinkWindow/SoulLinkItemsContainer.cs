﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkItemsContainer : MonoBehaviour
{
	[SerializeField]
	protected PoolsManager.PoolConfig _poolConfig;
	[SerializeField]
	protected SoulLinkItem _defaultItem;
	[SerializeField]
	protected ObjectPool<SoulLinkItem> _pool;
	[SerializeField]
	protected Transform _parent;

	public void Init()
	{
		if (_pool == null)
		{
			_pool = PoolsManager.Instance.CreatePool(_poolConfig, _defaultItem, _parent);
		}

		SoulLinkDataManager.Instance.OnSoulLinkItemAdded += Instance_OnSoulLinkItemAdded;

		CreateItems();
	}

	protected void CreateItems()
	{
		foreach (var soulItem in SoulLinkDataManager.Instance.Items)
		{
			Instance_OnSoulLinkItemAdded(soulItem);
		}
	}

	private void Instance_OnSoulLinkItemAdded(SoulLinkDataManager.SoulLinkPairDataItem obj)
	{
		var item = _pool.GetItem();
		item.Init(obj);
		item.transform.SetAsLastSibling();
	}

	[ContextMenu("Force")]
	private void ForceUpdate()
	{
		LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
	}
}
