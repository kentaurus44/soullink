﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkTeamItem : Item
{
	[SerializeField]
	protected Text _name;
	[SerializeField]
	protected Image _image;

	private string _itemName;

	public void Init(string item)
	{
		Clear();

		_itemName = item;

		Init();
	}

	protected override void UpdateStatic()
	{
		_name.text = _itemName;
	}

	protected override void UpdateDynamic() { }
	public override void Clear()
	{
		_name.text = string.Empty;
	}
}
