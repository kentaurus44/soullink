﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulLinkItem : Item
{
	[SerializeField]
	protected Text _item1;
	[SerializeField]
	protected Text _item2;
	[SerializeField]
	protected Text _location;
	[SerializeField]
	protected GameObject _player1FirstEncounter;
	[SerializeField]
	protected GameObject _player2FirstEncounter;

	private SoulLinkDataManager.SoulLinkPairDataItem _data;

	public void Init(SoulLinkDataManager.SoulLinkPairDataItem data)
	{
		_data = data;
		Init();
	}

	protected override void UpdateStatic()
	{
		_item1.text = _data.Item1;
		_item2.text = _data.Item2;
		_location.text = _data.Location;

		switch (_data.FirstEncounter)
		{
			case Player.None:
				_player1FirstEncounter.SetActive(false);
				_player2FirstEncounter.SetActive(false);
				break;
			case Player.Player1:
				_player1FirstEncounter.SetActive(true);
				_player2FirstEncounter.SetActive(false);
				break;
			case Player.Player2:
				_player1FirstEncounter.SetActive(false);
				_player2FirstEncounter.SetActive(true);
				break;
			default:
				break;
		}
	}

	protected override void UpdateDynamic() { }
	public override void Clear() { }
}