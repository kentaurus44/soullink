﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulLinkTeamContainer : MonoBehaviour
{
	[SerializeField]
	protected List<SoulLinkTeamItem> _player1Team = new List<SoulLinkTeamItem>();
	[SerializeField]
	protected List<SoulLinkTeamItem> _player2Team = new List<SoulLinkTeamItem>();

	public void Init()
	{
		SoulLinkDataManager.Instance.OnTeamUpdated += OnTeamUpdated;

		var team = SaveGameManager.Instance.SoulLink.Data.team;
		OnTeamUpdated(team);
	}

	private void OnTeamUpdated(List<SoulLinkDataManager.SoulLinkDataItem> team)
	{
		for (int i = 0; i < _player1Team.Count; ++i)
		{
			if (i < team.Count)
			{
				UpdateTeamSlot(i, team[i]);
			}
			else
			{
				SetSlotEmpty(i);
			}
		}
	}

	private void UpdateTeamSlot(int index, SoulLinkDataManager.SoulLinkDataItem item)
	{
		_player1Team[index].Init(item.Item1);
		_player2Team[index].Init(item.Item2);
	}

	private void SetSlotEmpty(int index)
	{
		_player1Team[index].Clear();
		_player2Team[index].Clear();
	}
}
