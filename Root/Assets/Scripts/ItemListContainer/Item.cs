﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
	[SerializeField]
	protected GameObject _container;

	protected void Init()
	{
		UpdateStatic();
	}

	protected abstract void UpdateStatic();
	protected abstract void UpdateDynamic();

	public abstract void Clear();
}
