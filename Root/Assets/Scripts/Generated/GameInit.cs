﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInit : MonoBehaviour
{
	void Start()
	{
		SaveGameManager.Instance.Init();
		SoulLinkDataManager.Instance.Init();

		Core.CustomCamera.CameraManager.Instance.Init();
		Core.Flow.FlowManager.Instance.Init();
	}
}
